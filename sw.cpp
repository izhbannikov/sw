/* 
 * File:   sw.cpp
 * Author: Ilya Y. Zhbannikov
 * 
 * Created on 6 Январь 2014 г., 15:39
 * 
 * This class realizes Smith-Waterman local alignment algorithm.
 */

#include "sw.h"

sw::sw() {
}

sw::sw(const sw& orig) {
}

sw::~sw() {
}

int **sw::MakeZero2DMatrix(int m, int n) {
    int **retval = new int*[m]; //m is a number of rows
    for(unsigned int i=0; i < n; ++i) { //n is a number of columns
        retval[i] = new int[n];
        for(unsigned int j = 0; j < n; ++j) {
            retval[i][j] = 0;
	}
    }
    
    return retval;
}


int sw::blosum62(char a, char b) {
    int i,j;
    
    //Look for a row
    switch(a) {
      case 'A' : i=0; break;
      case 'R' : i=1; break;
      case 'N' : i=2; break;
      case 'D' : i=3; break;
      case 'C' : i=4; break;
      case 'Q' : i=5; break;
      case 'E' : i=6; break;
      case 'G' : i=7; break;
      case 'H' : i=8; break;
      case 'I' : i=9; break;
      case 'L' : i=10; break;
      case 'K' : i=11; break;
      case 'M' : i=12; break;
      case 'F' : i=13; break;
      case 'P' : i=14; break;
      case 'S' : i=15; break;
      case 'T' : i=16; break;
      case 'W' : i=17; break;
      case 'Y' : i=18; break;
      case 'V' : i=19; break;
    }
    //Look for a column
    switch(b) {   
      case 'A' : j=0; break;
      case 'R' : j=1; break;
      case 'N' : j=2; break;
      case 'D' : j=3; break;
      case 'C' : j=4; break;
      case 'Q' : j=5; break;
      case 'E' : j=6; break;
      case 'G' : j=7; break;
      case 'H' : j=8; break;
      case 'I' : j=9; break;
      case 'L' : j=10; break;
      case 'K' : j=11; break;
      case 'M' : j=12; break;
      case 'F' : j=13; break;
      case 'P' : j=14; break;
      case 'S' : j=15; break;
      case 'T' : j=16; break;
      case 'W' : j=17; break;
      case 'Y' : j=18; break;
      case 'V' : j=19; break;
    }
    
    short blosum62[20][20] = {  { 4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0},
                                {-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3},
                                {-2,  0,  6,  1, -3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3},
                                {-2, -2,  1,  6, -3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3},
                                { 0, -3, -3, -3,  9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1},
                                {-1,  1,  0,  0, -3,  5,  2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2},
                                {-1,  0,  0,  2, -4,  2,  5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2},
                                { 0, -2,  0, -1, -3, -2, -2,  6, -2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3},
                                {-2,  0,  1, -1, -3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3},
                                {-1, -3, -3, -3, -1, -3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3},
                                {-1, -2, -3, -4, -1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1},
                                {-1,  2,  0, -1, -3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2},
                                {-1, -1, -2, -3, -1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1},
                                {-2, -3, -3, -3, -2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1},
                                {-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2},
                                { 1, -1,  1,  0, -1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2},
                                { 0, -1,  0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0},
                                {-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3},
                                {-2, -2, -2, -3, -2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1},
                                { 0, -3, -3, -3, -1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4}};
  
    
    return blosum62[i][j];
    
    /*
    switch(a) {
        case 'A' : i=0; break;
        case 'R' : i=1; break;
        case 'N' : i=2; break;
        case 'D' : i=3; break;
        case 'C' : i=4; break;
        case 'Q' : i=5; break;
        case 'E' : i=6; break;
        case 'G' : i=7; break;
        case 'H' : i=8; break;
        case 'I' : i=9; break;
        case 'L' : i=10; break;
        case 'K' : i=11; break;
        case 'M' : i=12; break;
        case 'F' : i=13; break;
        case 'P' : i=14; break;
        case 'S' : i=15; break;
        case 'T' : i=16; break;
        case 'W' : i=17; break;
        case 'Y' : i=18; break;
        case 'V' : i=19; break;
        //default : return;
    }

    //Ищем столбец
    switch(b) {
        case 'A' : j=0; break;
        case 'R' : j=1; break;
        case 'N' : j=2; break;
        case 'D' : j=3; break;
        case 'C' : j=4; break;
        case 'Q' : j=5; break;
        case 'E' : j=6; break;
        case 'G' : j=7; break;
        case 'H' : j=8; break;
        case 'I' : j=9; break;
        case 'L' : j=10; break;
        case 'K' : j=11; break;
        case 'M' : j=12; break;
        case 'F' : j=13; break;
        case 'P' : j=14; break;
        case 'S' : j=15; break;
        case 'T' : j=16; break;
        case 'W' : j=17; break;
        case 'Y' : j=18; break;
        case 'V' : j=19; break;
        //default : return;
    }

    int blosum62[20][20] = { {2, -2,  0,  0, -2,  0,  0,  1, -1, -1, -2, -1, -1, -3,  1,  1,  1, -6, -3,  0},
                {-2,  6,  0, -1, -4,  1, -1, -3,  2, -2, -3,  3,  0, -4,  0,  0, -1,  2, -4, -2},
                {0,  0,  2,  2, -4,  1,  1,  0,  2, -2, -3,  1, -2, -3,  0,  1,  0, -4, -2, -2},
                {0, -1,  2,  4, -5,  2,  3,  1,  1, -2, -4,  0, -3, -6, -1,  0,  0, -7, -4, -2},
                {-2, -4, -4, -5, 12, -5, -5, -3, -3, -2, -6, -5, -5, -4, -3,  0, -2, -8,  0, -2},
                {0,  1,  1,  2, -5,  4,  2, -1,  3, -2, -2,  1, -1, -5,  0, -1, -1, -5, -4, -2},
                {0, -1,  1,  3, -5,  2,  4,  0,  1, -2, -3,  0, -2, -5, -1,  0,  0, -7, -4, -2},
                {1, -3,  0,  1, -3, -1,  0,  5, -2, -3, -4, -2, -3, -5,  0,  1,  0, -7, -5, -1},
                {-1,  2,  2,  1, -3,  3,  1, -2,  6, -2, -2,  0, -2, -2,  0, -1, -1, -3,  0, -2},
                {-1, -2, -2, -2, -2, -2, -2, -3, -2,  5,  2, -2,  2,  1, -2, -1,  0, -5, -1,  4},
                {-2, -3, -3, -4, -6, -2, -3, -4, -2,  2,  6, -3,  4,  2, -3, -3, -2, -2, -1,  2},
                {-1,  3,  1,  0, -5,  1,  0, -2,  0, -2, -3,  5,  0, -5, -1,  0,  0, -3, -4, -2},
                {-1,  0, -2, -3, -5, -1, -2, -3, -2,  2,  4,  0,  6,  0, -2, -2, -1, -4, -2,  2},
                {-3, -4, -3, -6, -4, -5, -5, -5, -2,  1,  2, -5,  0,  9, -5, -3, -3,  0,  7, -1},
                {1,  0,  0, -1, -3,  0, -1,  0,  0, -2, -3, -1, -2, -5,  6,  1,  0, -6, -5, -1},
                {1,  0,  1,  0,  0, -1,  0,  1, -1, -1, -3,  0, -2, -3,  1,  2,  1, -2, -3, -1},
                {1, -1,  0,  0, -2, -1,  0,  0, -1,  0, -2,  0, -1, -3,  0,  1,  3, -5, -3,  0},
                {-6,  2, -4, -7, -8, -5, -7, -7, -3, -5, -2, -3, -4,  0, -6, -2, -5, 17,  0, -6},
                {-3, -4, -2, -4,  0, -4, -4, -5,  0, -1, -1, -4, -2,  7, -5, -3, -3,  0, 10, -2},
                {0, -2, -2, -2, -2, -2, -2, -1, -2,  4,  2, -2,  2, -1, -1, -1,  0, -6, -2,  4}};

    return blosum62[i][j];*/
    
}

int sw::match_score(char alpha, char beta, int gap_penalty) {
    if(alpha == '-' || beta == '-' ) {
        return gap_penalty;
    } 
    
    return blosum62(alpha, beta);
}


int sw::align(std::string seq1, std::string seq2, int ogp) {
    //ogp is an OPEN_GAP_PENALTY
    int m,n,i,j;
    m = seq1.length();
    n = seq2.length();
    
    int **score = MakeZero2DMatrix(m+1, n+1);
    
    for( i=0; i < m+1; ++i ) {
        score[i][0] = i*ogp;
    }

    for( j=0; j < n+1; j++ ) {
        score[0][j] = j*ogp;
    }
    
    int **pointer = MakeZero2DMatrix( m+1, n+1 ); //to store the traceback path
    
    int max_score = 0; //Initial maximum score in DP table
    //Calculate DP table and mark pointers
    int score_diagonal, score_up, score_left,max_i,max_j;
    max_i = max_j = 0;
    for(i=1; i < m + 1; ++i) {
     for(j=1; j < n + 1; ++j) {
      score_diagonal = score[i-1][j-1] + match_score( seq1[i-1], seq2[j-1], ogp );
      score_up = score[i][j-1] + ogp;
      score_left = score[i-1][j] + ogp;
      score[i][j] = std::max(0,std::max( score_left, std::max( score_up, score_diagonal) ) );
      if(score[i][j] == score_diagonal ) {
       pointer[i][j] = 3; //3 means trace diagonal
      }else if( score[i][j] == score_up ) {
       pointer[i][j] = 2; //2 means trace left
      }else if(score[i][j] == score_left ) {
       pointer[i][j] = 1; //1 means trace up
      }
      if( score[i][j] == 0 ) {
       pointer[i][j] = 0; //0 means end of the path
      }
      if(score[i][j] >= max_score ) {
       max_i = i;
       max_j = j;
       max_score = score[i][j];
      }
     }
    }
    std::string align1, align2; //aligned sequences
    
    
    i = max_i;
    j = max_j; //indices of path starting point
    std::cout << score[i][j] <<'\n';
    
    //Traceback, follow pointers:
    while( pointer[i][j] != 0 ) {
        if( pointer[i][j] == 3 ) {
            align1 =  seq1[i-1] + align1;
            align2 = seq2[j-1] + align2;
            i -= 1;
            j -= 1;
        } else if ( pointer[i][j] == 2 ) {
            align1 = '-' + align1;
	    align2 = seq2[j-1] + align2;
            j -= 1;
        } else if ( pointer[i][j] == 1 ) {
            align1 = seq1[i-1] + align1;
            align2 = '-' + align2;
	    i -= 1;
        }
    }
    std::cout << align1 << '\n' << align2 << '\n';
    
    return max_i-1;
}

