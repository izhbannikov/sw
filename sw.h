/* 
 * File:   sw.h
 * Author: kwt
 *
 * Created on 6 Январь 2014 г., 15:39
 */

#ifndef SW_H
#define	SW_H

#include <stdio.h>
#include <string.h>
#include <iostream>

class sw {
public:
    sw();
    sw(const sw& orig);
    
    int align(std::string s1, std::string s2, int ogp);
    int blosum62(char a, char b);
    
    virtual ~sw();
private:
    int **MakeZero2DMatrix(int m, int n);
    int match_score(char alpha, char beta, int gap_penalty);
};

#endif	/* SW_H */

